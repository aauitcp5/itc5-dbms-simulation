#include <iostream>
#include "common.h"
#include <math.h>

Point::Point() {
  setCoordinates(0, 0);
}

Point::Point(float x, float y) {
  setCoordinates(x, y);
}

void Point::setCoordinates(float x, float y) {
  _x = x;
  _y = y;
}

float Point::getX() const {
  return _x;
}

float Point::getY() const {
  return _y;
}

void Point::print() {
  std::cout << "The coordinates are: (" << _x << "," << _y << ")" << std::endl;
}

Line::Line(Point p1, Point p2) {
  _p1 = p1;
  _p2 = p2;
}

Point Line::getStartPoint() const {
  return _p1;
}

Point Line::getEndPoint() const {
  return _p2;
}

bool Line::operator == (const Line &l) {
  if((this->getStartPoint().getX() == l.getStartPoint().getX()) && (this->getStartPoint().getY() == l.getStartPoint().getY())
      &&
     (this->getEndPoint().getX() == l.getEndPoint().getX()) && (this->getEndPoint().getY() == l.getEndPoint().getY())) {
      return true;

  } else if((this->getStartPoint().getX() == l.getEndPoint().getX()) && (this->getStartPoint().getY() == l.getEndPoint().getY())
             &&
            (this->getEndPoint().getX() == l.getStartPoint().getX()) && (this->getEndPoint().getY() == l.getStartPoint().getY())) {
      return true;
  } else return false;
}

Matrix::Matrix(int rows, int columns) {
  // x == rows == cu ; y == columns == sensors
  _rows = rows; _columns = columns;
  _twoDimArr = std::vector<float>(rows*columns, -1.0);
}

void Matrix::putData(std::vector<float> data, int rows, int columns) {
  float avgData = averageData(data);
  _twoDimArr[(_columns*rows)+columns] = avgData;
}

float Matrix::averageData(std::vector<float> data) {
  float avgData = 0.0;
  for(float num : data)
    avgData += num; 
  avgData = avgData / data.size();
  return avgData;
}

// Here the _columns is used because it is the number of columns in the matrix
// and the second columns is the column that we want data from.
// The rows here should be the row that we want data from.
float Matrix::getData(int row, int column) {
  return _twoDimArr[(_columns * row)+column];
}

std::vector<CuSensorPair> Matrix::findMinPathLossCuSensorPairs() {
  std::vector<CuSensorPair> vecOfPairs;
  int currentSensor;
  int currentCu;
  float currentNumber;
  for(int i=0; i<_columns; i++) {
    currentNumber = pow(10, 20);
    for(int j=0; j<_rows; j++) {
      if(currentNumber > _twoDimArr[(j*_columns + i)]) {
        currentNumber = _twoDimArr[(j*_columns + i)];
        currentCu = j;
        currentSensor = i;
      } 
    }
    CuSensorPair csPair;
    csPair._cuID = currentCu;
    csPair._epID._cuID = -1;
    csPair._epID._epIndex = currentSensor;
    vecOfPairs.push_back(csPair);
  }
  return vecOfPairs;
}
