#include "common.h"
#include "room.h"
#include "placeable.h"
#include <iostream>

/*
void testScenario() { 
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++\nSimulator output: \n" << std::endl;
  Building myBuilding = Building(Border(Point(-30, 30), Point(30, -30)));
  Building* buildingPointer = &myBuilding;
  myBuilding.addRoom(Point(-10,12), Point(0,2), Point(-7,3), buildingPointer);   //Room A -> index 0
  myBuilding.addRoom(Point(0,12), Point(10,2), Point(3,3), buildingPointer);     //Room B -> index 1
  myBuilding.addRoom(Point(-10,0), Point(0,-10), Point(-3,-1), buildingPointer); //Room C -> index 2
  myBuilding.addRoom(Point(0,0), Point(10,-10), Point(7,-1), buildingPointer);   //Room D -> index 3

  myBuilding.getRoomByIndex(0)->addSensor(Point(-3,5), 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-8,11), 0);
  myBuilding.getRoomByIndex(1)->addSensor(Point(3,5), 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(1,11), 1);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-3,-5), 2);
  myBuilding.getRoomByIndex(3)->addSensor(Point(5,-8), 3);

  myBuilding.createWallStructure();

  myBuilding.restartServer(&myBuilding); //This is a neccessary function to run!

  // From here on out, the simulation of a scenario can begin:
  for(int i=0; i<myBuilding.getNumberOfRooms(); i++) {
    ControlUnit* currentCU = myBuilding.getCUByIndex(i);
    currentCU->run();
  }
  myBuilding._server.run();
  
  // foreach cu, call .run()
  // server.findMaxCuSensorPairs
  // server call each cu and give it its sensor pairs (New function)

  myBuilding.getRoomByIndex(0)->getBorder().getUpperLeftCorner();
  myBuilding.getRoomByIndex(0)->getSensors();
  // std::cout << "In between!" << std::endl;

  std::cout << "Control unit: " << std::endl;
  myBuilding.getCUByIndex(3)->getLocation().print();

  std::cout << "\nSensor: " << std::endl;
  myBuilding.getRoomByIndex(1)->getSensors().at(0).getLocation().print();

  std::cout << "\nThe number of walls between the two points are: " 
  << myBuilding.intersectWithBorders(myBuilding.getCUByIndex(3)->getLocation(), myBuilding.getRoomByIndex(1)->getSensors().at(0).getLocation()) 
  << std::endl;
  


  std::cout << "\nControl unit: " << std::endl;
  myBuilding.getCUByIndex(3)->getLocation().print();

  std::cout << "\nSensor: " << std::endl;
  myBuilding.getRoomByIndex(0)->getSensors().at(1).getLocation().print();

  std::cout << "\nThe number of walls between the two points are: " 
  << myBuilding.intersectWithBorders(myBuilding.getCUByIndex(3)->getLocation(), myBuilding.getRoomByIndex(0)->getSensors().at(1).getLocation()) 
  << std::endl;

  std::cout << "\nRSSI calculation test: " << myBuilding.getCUByIndex(0)->calculatePathLoss(&myBuilding.getRoomByIndex(2)->getSensors().at(0)) << std::endl;

  std::cout << "End of simulator output: \n+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}
*/

void testScenario2(int numberOfRuns) {
  // Setting up scenario: 
  Building myBuilding = Building(Border(Point(-30.0, 30.0), Point(30.0, -30.0)));
  Building* buildingPointer = &myBuilding;
  myBuilding.addRoom(Point(-10.0,12.0), Point(0.0,2.0), Point(-7.0,3.0), buildingPointer);   //Room A -> index 0
  myBuilding.addRoom(Point(0.0,12.0), Point(10.0,2.0), Point(3.0,3.0), buildingPointer);     //Room B -> index 1
  myBuilding.addRoom(Point(-10.0,0.0), Point(0.0,-10.0), Point(-3.0,-1.0), buildingPointer); //Room C -> index 2
  myBuilding.addRoom(Point(0.0,0.0), Point(10.0,-10.0), Point(7.0,-1.0), buildingPointer);   //Room D -> index 3

  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.0,5.0), 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-8.0,11.0), 0);
  myBuilding.getRoomByIndex(1)->addSensor(Point(3.0,5.0), 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(1.0,11.0), 1);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-3.0,-5.0), 2);
  myBuilding.getRoomByIndex(3)->addSensor(Point(5.0,-8.0), 3);

  myBuilding.createWallStructure();

  myBuilding.restartServer(&myBuilding); //This is a neccessary function to run!
  // Scenario setup complete
  // From here on out, the simulation of a scenario can begin:
  float sumOfAccuracies = 0;
  for(int i=0; i<numberOfRuns; i++) {
    for(int i=0; i<myBuilding.getNumberOfRooms(); i++) {
      ControlUnit* currentCU = myBuilding.getCUByIndex(i);
      currentCU->run();
    }
    myBuilding._server.run();
    // std::cout << "Test" << std::endl;
    sumOfAccuracies += myBuilding._server.checkAccuracyOfCsPairs();
  }
  float averageAccuracy = sumOfAccuracies / (float) numberOfRuns;
  std::cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++\nThe final values:" << std::endl;
  std::cout << "\n\nThe average accuracy is: " << averageAccuracy << "%"<< std::endl;
  myBuilding.averageDistance();
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}

void scenarioWithoutHall(int numberOfRuns) {
  // Setting up scenario: 
  Building myBuilding = Building(Border(Point(-30.0, 30.0), Point(30.0, -30.0)));
  Building* buildingPointer = &myBuilding;
  myBuilding.addRoom(Point(-10.0,12.0), Point(0-0,2.0)   , Point(-7.0,3.0) , buildingPointer); //Room A -> index 0
  myBuilding.addRoom(Point(0.0,12.0)  , Point(10.0,2.0)  , Point(3.0,3.0)  , buildingPointer); //Room B -> index 1
  myBuilding.addRoom(Point(-10.0,0.0) , Point(0.0,-10.0) , Point(-3.0,-1.0), buildingPointer); //Room C -> index 2
  myBuilding.addRoom(Point(0.0,0.0)   , Point(10.0,-10.0), Point(7.0,-1.0) , buildingPointer); //Room D -> index 3

  myBuilding.getRoomByIndex(0)->addSensor(Point(-9.0,4.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-9.0,9.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-8.0,11.0), 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.0,5.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-1.0,8.0) , 0);

  myBuilding.getRoomByIndex(1)->addSensor(Point(1.0,6.0)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(1.0,11.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(3.0,3.0)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(5.0,10.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(9.0,6.0)  , 1);

  myBuilding.getRoomByIndex(2)->addSensor(Point(-9.0,-1.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-7.0,-8.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-6.0,-4.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-3.0,-5.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-2.0,-4.0), 2);

  myBuilding.getRoomByIndex(3)->addSensor(Point(2.0,-9.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,-8.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,-3.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(7.0,-4.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(9.0,-6.0) , 3);

  myBuilding.createWallStructure();

  myBuilding.restartServer(&myBuilding); //This is a neccessary function to run!
  // Scenario setup complete
  // From here on out, the simulation of a scenario can begin:
  float sumOfAccuracies = 0;
  float totalNumberOfNotCorrectlySetRooms = 0;
  for(int i=0; i<numberOfRuns; i++) {
    for(int i=0; i<myBuilding.getNumberOfRooms(); i++) {
      ControlUnit* currentCU = myBuilding.getCUByIndex(i);
      currentCU->run();
    }
    myBuilding._server.run();
    // std::cout << "Test" << std::endl;
    sumOfAccuracies += myBuilding._server.checkAccuracyOfCsPairs();
    totalNumberOfNotCorrectlySetRooms += (float) myBuilding._server.checkNumberOfImproperlySetRooms();
  }
  float averageAccuracy = sumOfAccuracies / (float) numberOfRuns;
  float averageNumberOfNotCorrectlySetRooms = totalNumberOfNotCorrectlySetRooms / (float) numberOfRuns;
  std::cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++\nThe final values of scenario without hall:" << std::endl;
  std::cout << "\n\nThe average accuracy is: " << averageAccuracy << "%"<< std::endl;
  myBuilding.averageDistance();
  std::cout << "The average number of rooms not correctly set is: " << averageNumberOfNotCorrectlySetRooms << std::endl;
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}

void scenarioWithHall(int numberOfRuns) {
  // Setting up scenario: 
  Building myBuilding = Building(Border(Point(-30.0, 30.0), Point(30.0, -30.0)));
  Building* buildingPointer = &myBuilding;
  myBuilding.addRoom(Point(-10.0,12.0), Point(0-0,2.0)   , Point(-7.0,3.0) , buildingPointer); //Room A -> index 0
  myBuilding.addRoom(Point(0.0,12.0)  , Point(10.0,2.0)  , Point(3.0,3.0)  , buildingPointer); //Room B -> index 1
  myBuilding.addRoom(Point(-10.0,0.0) , Point(0.0,-10.0) , Point(-3.0,-1.0), buildingPointer); //Room C -> index 2
  myBuilding.addRoom(Point(0.0,0.0)   , Point(10.0,-10.0), Point(7.0,-1.0) , buildingPointer); //Room D -> index 3
  myBuilding.addRoom(Point(-10.0,2.0) , Point(10.0,0.0)  , Point(1.0,1.0)  , buildingPointer); //Room D -> index 3

  myBuilding.getRoomByIndex(0)->addSensor(Point(-9.0,4.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-9.0,9.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-8.0,11.0), 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.0,5.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-1.0,8.0) , 0);

  myBuilding.getRoomByIndex(1)->addSensor(Point(1.0,6.0)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(1.0,11.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(3.0,3.0)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(5.0,10.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(9.0,6.0)  , 1);

  myBuilding.getRoomByIndex(2)->addSensor(Point(-9.0,-1.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-7.0,-8.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-6.0,-4.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-3.0,-5.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-2.0,-4.0), 2);

  myBuilding.getRoomByIndex(3)->addSensor(Point(2.0,-9.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,-8.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,-3.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(7.0,-4.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(9.0,-6.0) , 3);

  myBuilding.getRoomByIndex(4)->addSensor(Point(-9.0,1.0) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-7.0,1.0) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-2.0,1.0) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(3.0,1.0)  , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(6.0,1.0)  , 4);

  myBuilding.createWallStructure();

  myBuilding.restartServer(&myBuilding); //This is a neccessary function to run!
  // Scenario setup complete
  // From here on out, the simulation of a scenario can begin:
  float sumOfAccuracies = 0;
  float totalNumberOfNotCorrectlySetRooms = 0;
  for(int i=0; i<numberOfRuns; i++) {
    for(int i=0; i<myBuilding.getNumberOfRooms(); i++) {
      ControlUnit* currentCU = myBuilding.getCUByIndex(i);
      currentCU->run();
    }
    myBuilding._server.run();
    // std::cout << "Test" << std::endl;
    sumOfAccuracies += myBuilding._server.checkAccuracyOfCsPairs();
    totalNumberOfNotCorrectlySetRooms += (float) myBuilding._server.checkNumberOfImproperlySetRooms();
  }
  float averageAccuracy = sumOfAccuracies / (float) numberOfRuns;
  float averageNumberOfNotCorrectlySetRooms = totalNumberOfNotCorrectlySetRooms / (float) numberOfRuns;
  std::cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++\nThe final values of scenario with hall:" << std::endl;
  std::cout << "\n\nThe average accuracy is: " << averageAccuracy << "%"<< std::endl;
  myBuilding.averageDistance();
  std::cout << "The average number of rooms not correctly set is: " << averageNumberOfNotCorrectlySetRooms << std::endl;
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}

// for this scenario I have momentarily changed the attenuation per wall to zero.
// when this scenario is not run, please check that it is set correctly!
// Cu1 is moved to the upper right corner, the third sensor in room1 is moved to 2,2
//
// With these changes and the lossPrWall set to zero, we have a few somewhat consistent
// errors, but they are not totally guarenteed.
//
// The error seems to be much more consistent if lossPrWall is set to one.
void definiteErrorScenario(int numberOfRuns) {
  // Setting up scenario: 
  Building myBuilding = Building(Border(Point(-30.0, 30.0), Point(30.0, -30.0)));
  Building* buildingPointer = &myBuilding;
  myBuilding.addRoom(Point(-10.0,12.0), Point(0-0,2.0)   , Point(-7.0,3.0) , buildingPointer); //Room A -> index 0
  myBuilding.addRoom(Point(0.0,12.0)  , Point(10.0,2.0)  , Point(10.0,12.0)  , buildingPointer); //Room B -> index 1
  myBuilding.addRoom(Point(-10.0,0.0) , Point(0.0,-10.0) , Point(-3.0,-1.0), buildingPointer); //Room C -> index 2
  myBuilding.addRoom(Point(0.0,0.0)   , Point(10.0,-10.0), Point(7.0,-1.0) , buildingPointer); //Room D -> index 3
  myBuilding.addRoom(Point(-10.0,2.0) , Point(10.0,0.0)  , Point(1.0,1.0)  , buildingPointer); //Room D -> index 3

  myBuilding.getRoomByIndex(0)->addSensor(Point(-9.0,4.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-9.0,9.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-8.0,11.0), 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.0,5.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-1.0,8.0) , 0);

  myBuilding.getRoomByIndex(1)->addSensor(Point(1.0,6.0)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(1.0,11.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(3.0,3.0)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(5.0,10.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(9.0,6.0)  , 1);

  myBuilding.getRoomByIndex(2)->addSensor(Point(-9.0,-1.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-7.0,-8.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-6.0,-4.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-3.0,-5.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(-2.0,-4.0), 2);

  myBuilding.getRoomByIndex(3)->addSensor(Point(2.0,-9.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,-8.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,-3.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(7.0,-4.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(9.0,-6.0) , 3);

  myBuilding.getRoomByIndex(4)->addSensor(Point(-9.0,1.0) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-7.0,1.0) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-2.0,1.0) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(3.0,1.0)  , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(6.0,1.0)  , 4);

  myBuilding.createWallStructure();

  myBuilding.restartServer(&myBuilding); //This is a neccessary function to run!
  // Scenario setup complete
  // From here on out, the simulation of a scenario can begin:
  float sumOfAccuracies = 0;
  for(int i=0; i<numberOfRuns; i++) {
    for(int i=0; i<myBuilding.getNumberOfRooms(); i++) {
      ControlUnit* currentCU = myBuilding.getCUByIndex(i);
      currentCU->run();
    }
    myBuilding._server.run();
    // std::cout << "Test" << std::endl;
    sumOfAccuracies += myBuilding._server.checkAccuracyOfCsPairs();
  }
  float averageAccuracy = sumOfAccuracies / (float) numberOfRuns;
  std::cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++\nThe final values of scenario \"definiteError\":" << std::endl;
  std::cout << "\n\nThe average accuracy is: " << averageAccuracy << "%"<< std::endl;
  myBuilding.averageDistance();
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}

void scenarioSmallRoomsWithoutHall(int numberOfRuns) {
  // Setting up scenario: 
  Building myBuilding = Building(Border(Point(-30.0, 30.0), Point(30.0, -30.0)));
  Building* buildingPointer = &myBuilding;
  myBuilding.addRoom(Point(-4.0,6.0), Point(-2.0,2.0)  , Point(-3.5, 2.5) , buildingPointer); //Room A -> index 0
  myBuilding.addRoom(Point(-2.0,6.0), Point(0.0,2.0)   , Point(-1.5, 2.5)  , buildingPointer); //Room B -> index 1
  myBuilding.addRoom(Point(0.0,6.0) , Point(2.0,2.0)   , Point( 0.5, 2.5), buildingPointer); //Room C -> index 2
  myBuilding.addRoom(Point(2.0,6.0) , Point(4.0,2.0)   , Point( 2.5, 2.5) , buildingPointer); //Room D -> index 3
  myBuilding.addRoom(Point(-4.0,0.0), Point(-2.0,-4.0) , Point(-2.5,-0.5)  , buildingPointer); //Room D -> index 3
  myBuilding.addRoom(Point(-2.0,0.0), Point(0.0,-4.0)  , Point(-0.5,-0.5)  , buildingPointer); //Room D -> index 3
  myBuilding.addRoom(Point(0.0,0.0) , Point(2.0,-4.0)  , Point( 1.5,-0.5)  , buildingPointer); //Room D -> index 3
  myBuilding.addRoom(Point(2.0,0.0) , Point(4.0,-4.0)  , Point( 3.5,-0.5)  , buildingPointer); //Room D -> index 3

  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.5,3.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-2.5,2.8) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-2.7,5.0), 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-2.2,5.5) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.0,5.7) , 0);

  myBuilding.getRoomByIndex(1)->addSensor(Point(-1.5,2.15)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-0.7,3.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-1.0,4.5)  , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-1.9,5.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-0.5,5.9)  , 1);

  myBuilding.getRoomByIndex(2)->addSensor(Point(1.0,2.3), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(1.5,2.5), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(0.5, 3.5), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(1.9,5.0), 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(1.0,5.5), 2);

  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,3.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.5,3.5) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(2.5,4.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,5.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.5,5.5) , 3);

  myBuilding.getRoomByIndex(4)->addSensor(Point(-2.2,-0.5) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-2.5,-1.2) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-3.5,-1.7) , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-3.0,-2.6)  , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-3.5,-3.5)  , 4);

  myBuilding.getRoomByIndex(5)->addSensor(Point(-1.5,-0.3) , 4);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-1.4,-2.0) , 4);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-0.3,-3.0) , 4);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-1.0,-3.3)  , 4);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-0.5,-3.8)  , 4);

  myBuilding.getRoomByIndex(6)->addSensor(Point(0.8,-0.5) , 4);
  myBuilding.getRoomByIndex(6)->addSensor(Point(0.1,-1.0) , 4);
  myBuilding.getRoomByIndex(6)->addSensor(Point(1.4,-2.0) , 4);
  myBuilding.getRoomByIndex(6)->addSensor(Point(0.5,-2.5)  , 4);
  myBuilding.getRoomByIndex(6)->addSensor(Point(0.5,-3.8)  , 4);

  myBuilding.getRoomByIndex(7)->addSensor(Point(3.0,-0.2) , 4);
  myBuilding.getRoomByIndex(7)->addSensor(Point(3.2,-1.0) , 4);
  myBuilding.getRoomByIndex(7)->addSensor(Point(3.7,-1.5) , 4);
  myBuilding.getRoomByIndex(7)->addSensor(Point(3.0,-3.2)  , 4);
  myBuilding.getRoomByIndex(7)->addSensor(Point(2.5,-3.4)  , 4);

  myBuilding.createWallStructure();

  myBuilding.restartServer(&myBuilding); //This is a neccessary function to run!
  // Scenario setup complete
  // From here on out, the simulation of a scenario can begin:
  float sumOfAccuracies = 0;
  float totalNumberOfNotCorrectlySetRooms = 0;
  for(int i=0; i<numberOfRuns; i++) {
    for(int i=0; i<myBuilding.getNumberOfRooms(); i++) {
      ControlUnit* currentCU = myBuilding.getCUByIndex(i);
      currentCU->run();
    }
    myBuilding._server.run();
    // std::cout << "Test" << std::endl;
    sumOfAccuracies += myBuilding._server.checkAccuracyOfCsPairs();
    totalNumberOfNotCorrectlySetRooms += (float) myBuilding._server.checkNumberOfImproperlySetRooms();
  }
  float averageAccuracy = sumOfAccuracies / (float) numberOfRuns;
  float averageNumberOfNotCorrectlySetRooms = totalNumberOfNotCorrectlySetRooms / (float) numberOfRuns;
  std::cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++\nThe final values of small room scenario without hall:" << std::endl;
  std::cout << "\n\nThe average accuracy is: " << averageAccuracy << "%"<< std::endl;
  myBuilding.averageDistance();
  std::cout << "The average number of rooms not correctly set is: " << averageNumberOfNotCorrectlySetRooms << std::endl;
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}

void scenarioSmallRoomsWithHall(int numberOfRuns) {
  // Setting up scenario: 
  Building myBuilding = Building(Border(Point(-30.0, 30.0), Point(30.0, -30.0)));
  Building* buildingPointer = &myBuilding;

  myBuilding.addRoom(Point(-4.0,6.0), Point(-2.0,2.0)  , Point(-3.5, 2.5)  , buildingPointer); 
  myBuilding.addRoom(Point(-2.0,6.0), Point(0.0,2.0)   , Point(-1.5, 2.5)  , buildingPointer); 
  myBuilding.addRoom(Point(0.0,6.0) , Point(2.0,2.0)   , Point( 0.5, 2.5)  , buildingPointer); 
  myBuilding.addRoom(Point(2.0,6.0) , Point(4.0,2.0)   , Point( 2.5, 2.5)  , buildingPointer); 
  myBuilding.addRoom(Point(-4.0,0.0), Point(-2.0,-4.0) , Point(-2.5,-0.5)  , buildingPointer); 
  myBuilding.addRoom(Point(-2.0,0.0), Point(0.0,-4.0)  , Point(-0.5,-0.5)  , buildingPointer); 
  myBuilding.addRoom(Point(0.0,0.0) , Point(2.0,-4.0)  , Point( 1.5,-0.5)  , buildingPointer); 
  myBuilding.addRoom(Point(2.0,0.0) , Point(4.0,-4.0)  , Point( 3.5,-0.5)  , buildingPointer); 
  myBuilding.addRoom(Point(-4.0,2.0), Point(4.0, 0.0)  , Point( 3.5, 1.7)  , buildingPointer); 

  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.5,3.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-2.5,2.8) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-2.7,5.0) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-2.2,5.5) , 0);
  myBuilding.getRoomByIndex(0)->addSensor(Point(-3.0,5.7) , 0);

  myBuilding.getRoomByIndex(1)->addSensor(Point(-1.5,2.15), 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-0.7,3.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-1.0,4.5) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-1.9,5.0) , 1);
  myBuilding.getRoomByIndex(1)->addSensor(Point(-0.5,5.9) , 1);

  myBuilding.getRoomByIndex(2)->addSensor(Point(1.0,2.3)  , 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(1.5,2.5)  , 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(0.5, 3.5) , 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(1.9,5.0)  , 2);
  myBuilding.getRoomByIndex(2)->addSensor(Point(1.0,5.5)  , 2);

  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,3.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.5,3.5) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(2.5,4.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.0,5.0) , 3);
  myBuilding.getRoomByIndex(3)->addSensor(Point(3.5,5.5) , 3);

  myBuilding.getRoomByIndex(4)->addSensor(Point(-2.2,-0.5)  , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-2.5,-1.2)  , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-3.5,-1.7)  , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-3.0,-2.6)  , 4);
  myBuilding.getRoomByIndex(4)->addSensor(Point(-3.5,-3.5)  , 4);

  myBuilding.getRoomByIndex(5)->addSensor(Point(-1.5,-0.3)  , 5);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-1.4,-2.0)  , 5);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-0.3,-3.0)  , 5);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-1.0,-3.3)  , 5);
  myBuilding.getRoomByIndex(5)->addSensor(Point(-0.5,-3.8)  , 5);

  myBuilding.getRoomByIndex(6)->addSensor(Point(0.8,-0.5)   , 6);
  myBuilding.getRoomByIndex(6)->addSensor(Point(0.1,-1.0)   , 6);
  myBuilding.getRoomByIndex(6)->addSensor(Point(1.4,-2.0)   , 6);
  myBuilding.getRoomByIndex(6)->addSensor(Point(0.5,-2.5)   , 6);
  myBuilding.getRoomByIndex(6)->addSensor(Point(0.5,-3.8)   , 6);

  myBuilding.getRoomByIndex(7)->addSensor(Point(3.0,-0.2)  , 7);
  myBuilding.getRoomByIndex(7)->addSensor(Point(3.2,-1.0)  , 7);
  myBuilding.getRoomByIndex(7)->addSensor(Point(3.7,-1.5)  , 7);
  myBuilding.getRoomByIndex(7)->addSensor(Point(3.0,-3.2)  , 7);
  myBuilding.getRoomByIndex(7)->addSensor(Point(2.5,-3.4)  , 7);

  myBuilding.getRoomByIndex(8)->addSensor(Point(-3.6, 1.0)  , 8);
  myBuilding.getRoomByIndex(8)->addSensor(Point(-3.0, 0.8)  , 8);
  myBuilding.getRoomByIndex(8)->addSensor(Point(-1.0, 1.0)  , 8);
  myBuilding.getRoomByIndex(8)->addSensor(Point( 0.0, 1.8)  , 8);
  myBuilding.getRoomByIndex(8)->addSensor(Point( 3.0, 0.4)  , 8);

  myBuilding.createWallStructure();

  myBuilding.restartServer(&myBuilding); //This is a neccessary function to run!
  // Scenario setup complete
  // From here on out, the simulation of a scenario can begin:
  float sumOfAccuracies = 0;
  float totalNumberOfNotCorrectlySetRooms = 0;
  for(int i=0; i<numberOfRuns; i++) {
    for(int i=0; i<myBuilding.getNumberOfRooms(); i++) {
      ControlUnit* currentCU = myBuilding.getCUByIndex(i);
      currentCU->run();
    }
    myBuilding._server.run();
    // std::cout << "Test" << std::endl;
    sumOfAccuracies += myBuilding._server.checkAccuracyOfCsPairs();
    totalNumberOfNotCorrectlySetRooms += (float) myBuilding._server.checkNumberOfImproperlySetRooms();
  }
  float averageAccuracy = sumOfAccuracies / (float) numberOfRuns;
  float averageNumberOfNotCorrectlySetRooms = totalNumberOfNotCorrectlySetRooms / (float) numberOfRuns;
  std::cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++\nThe final values of small room scenario with hall:" << std::endl;
  std::cout << "\n\nThe average accuracy is: " << averageAccuracy << "%"<< std::endl;
  myBuilding.averageDistance();
  std::cout << "The average number of rooms not correctly set is: " << averageNumberOfNotCorrectlySetRooms << std::endl;
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}

int main() {
  std::cout << "+++++++++++++++++++++++++++++++++++++++++++++++++\nSimulator output: \n" << std::endl;
  int numberOfRuns = 100;
  scenarioWithoutHall(numberOfRuns);
  scenarioWithHall(numberOfRuns);
  scenarioSmallRoomsWithoutHall(numberOfRuns);
  scenarioSmallRoomsWithHall(numberOfRuns);
  std::cout << "\n\nEnd of simulator output: \n+++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
  return 0;
}
