#ifndef COMMON_H
#define COMMON_H

#include <vector>

struct endPointID {
  int _cuID, _epIndex;
};

struct CuSensorPair {
  int _cuID;
  endPointID _epID;
};

enum endPointType {actuator, sensor};

class Point {
  private:
    void setCoordinates(float, float);
    float _x, _y;

  public:
    Point();
    Point(float, float);
    float getX() const;
    float getY() const;
    void print();
};

class Line {
  private:
    Point _p1, _p2;

  public:
    Line(Point, Point);
    bool operator == (const Line &RHS);
    Point getStartPoint() const;
    Point getEndPoint() const;
};

class Matrix {
  private:
    int _rows, _columns;
    std::vector<float> _twoDimArr;
    float averageData(std::vector<float>);

  public:
    Matrix(){}; // Do not use the default constructor!
    Matrix(int, int);
    void putData(std::vector<float>, int, int);
    float getData(int, int);
    std::vector<CuSensorPair> findMinPathLossCuSensorPairs();
};

#endif
