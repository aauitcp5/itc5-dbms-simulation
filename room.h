#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <vector>
#include "common.h"
#include "placeable.h"

class ControlUnit;
class Sensor;
class Actuator;
class Building;

class Border {
  private:
    Point _upperLeftCorner;
    Point _lowerRightCorner;
  public:
    Border(){}; // Do not use the default constructor!
    Border(Point, Point);
    Point getUpperLeftCorner();
    Point getLowerRightCorner();
};

class Room {
  private:
    int _ID;
    float _actualTemperature;
    Border _roomBorder;
    Building* _building;
    std::vector<Actuator> _actuatorContainer;
    std::vector<int> _sensorIDs;
  public:
    Room(Point, Point, int, Building*);
    void addActuator(Actuator);
    void addActuator(Point, int);
    void addSensor(Point, int);
    std::vector<int> getSensors();
    float getTemperature();
    Border getBorder();
    int getID();
};

class Server {
  private:
    Building* _building;
    Matrix _avgRssiValues;
    std::vector<CuSensorPair> _csPairs;
    void processData();
    void communicateNewCsPairs();
  public:
    Server(){}; // Do not use the default constructor!
    Server(Building*);
    void putPathLossData(std::vector<float>, int, int); //data, cuID, sensorID
    void run();
    int checkNumberOfImproperlySetRooms();
    float checkAccuracyOfCsPairs();
};

class Building {
  private:
    std::vector<ControlUnit> _controlUnits;
    std::vector<Room> _rooms;
    std::vector<Sensor> _sensors;
    std::vector<std::vector<float>> _sensorData;
    std::vector<Line> _walls;
    std::vector<float> _distances;
    Border _border;
    int direction(Point, Point, Point);
    bool doLinesIntersect(Line, Line);
    void extractLinesFromBorder(Border);
  public:
    Server _server;
    //Building(); //Do not use!
    Building(Border);
    void printNumberOfWalls();
    void addRoom(Point, Point, Point, Building*);
    void addControlUnit(ControlUnit);
    void readSensors();
    void createWallStructure();
    void restartServer(Building*);
    void addSensor(Sensor);
    void addDistance(float);
    int getNumberOfRooms();
    int intersectWithBorders(Point, Point);
    int getNumberOfSensors();
    float distanceBetweenPoints(Point, Point);
    void averageDistance();
    Room* getRoomByIndex(int);
    std::vector<Sensor> getSensors();
    ControlUnit* getCUByIndex(int);
};

#endif
