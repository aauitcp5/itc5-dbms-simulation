# Specify compiler
CC=g++ -std=c++17
  
# Specify linker
LINK=g++
  
# Link the object files and dependent libraries into a binary
simulator : simulator.o placeable.o common.o room.o
	$(LINK) simulator.o placeable.o common.o room.o -o simulator

# Compile the source files into object files
simulator.o : simulator.cpp
	$(CC) -c simulator.cpp 

placeable.o : placeable.cpp
	$(CC) -c placeable.cpp 

room.o : room.cpp
	$(CC) -c room.cpp 

common.o : common.cpp
	$(CC) -c common.cpp

# Clean target
clean :
		rm *.o simulator 
