#include <iostream>
#include <random>
#include <stdlib.h>
#include <vector>
#include "room.h"
#include "placeable.h"
#include "common.h"

Placeable::Placeable(Point location) {
  _location = location;
}

Placeable::Placeable() {
  _location = Point();
}

Point Placeable::getLocation() const {
  return _location;
}

float ControlUnit::calculatePathLoss(Placeable *placeable) {
  Point placeableLocation = placeable->getLocation();
  float dist = _building->distanceBetweenPoints(placeableLocation, this->getLocation());
  _building->addDistance(dist);
  int wallsBetweenPoints = _building->intersectWithBorders(placeableLocation, this->getLocation());
  std::random_device rd; // This is needed to create a random seed for the generator
  std::default_random_engine generator;
  generator.seed(rd()); // This is where the seed is chosen randomly
  float sigma = 2.6; // The right value is 2.6
  float alpha = 0.405;
  std::normal_distribution<float> wallGauss{5.5, 3.5}; float lossPrWall = wallGauss(generator);
  // float lossPrWall = 0;
  // float lossPrWall = 1;
  // float lossPrWall = 5.5; // the unit is dB
  std::normal_distribution<float> gauss{0,sigma};
  float pathLoss = alpha * dist + gauss(generator) + (wallsBetweenPoints * lossPrWall);
  return pathLoss;
}

std::vector<float> ControlUnit::getPathLossWithEndpoint(Placeable *placeable, int numberOfMeasurements) {
  std::vector<float> vec;
  for(int i=0; i<numberOfMeasurements; i++)
    vec.push_back(calculatePathLoss(placeable));
  return vec;
}

void ControlUnit::run() {
  int numberOfMeasurements = 10;
  std::vector<Sensor> sensors = _building->getSensors();
  std::vector<float> pathLoss;
  for(Sensor s : sensors) {
    pathLoss = getPathLossWithEndpoint(&s, numberOfMeasurements);
    // std::cout << "The path loss between cu " << _cuID << " and sensor "<< s.getID()._epIndex << " is: "<< pathLoss[0] << std::endl;
    _sensorData.push_back(pathLoss);
  }
  // std::cout << std::endl;
  for(int j=0; j<(int)_sensorData.size(); j++) {
    _building->_server.putPathLossData(_sensorData.at(j), _cuID, j);
  }
  _sensorData.clear();
  // for(int j=_sensorData.size()-1; !_sensorData.empty(); j--) {
    // std::cout << _cuID << std::endl;
    // _building->_server.putPathLossData(_sensorData.at(j), _cuID, j);
    // _sensorData.pop_back();
  // }
}

void ControlUnit::setConnectedSensors(std::vector<endPointID> sensors) {
  for(int i=0; i<(int)sensors.size(); i++) {
    _connectedSensors.push_back(sensors.at(i));
  }
}

void ControlUnit::setConnectedSensor(endPointID sensor) {
  _connectedSensors.push_back(sensor);
}

endPointID Endpoint::getID() {
  return _ID;
}

void Endpoint::setID(int cuID, int epID) {
  _ID._cuID = cuID;
  _ID._epIndex = epID;
}

Building* Endpoint::getBuilding() {
  return _building;
}

void Actuator::setState(char state) {
  _state = state;
}

char Actuator::getState() {
  return _state;
}

float Sensor::getTemperature() {
  float actualTemp = getBuilding()->getRoomByIndex(getID()._cuID)->getTemperature();
  // float actualTemp = _parentRoom->getTemperature(); // This is how we used to do it
  float measuredTemp = actualTemp + (rand() % 21 - 10)/10; //This rand() should ideally follow a Gaussian distribution. I believe it is now a uniform distribution.
  return measuredTemp;
}


