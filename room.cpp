#include <math.h>
#include <iostream>
#include <ostream>
#include <map>
#include "common.h"
#include "placeable.h"
#include "room.h"

Room::Room(Point upperLeftRoom, Point lowerRightRoom, int ID, Building* building) {
  _building = building;
  _roomBorder = Border(upperLeftRoom, lowerRightRoom);
  _ID = ID;
  _actualTemperature = 20.0;
}

void Room::addActuator(Actuator actuator) {
  _actuatorContainer.push_back(actuator);
}

void Room::addActuator(Point actuatorPosition, int cuID) {
  int actuatorID = _actuatorContainer.size();
  Actuator actuator = Actuator(actuatorPosition, cuID, actuatorID);
  _actuatorContainer.push_back(actuator);
}

void Room::addSensor(Point sensorPosition, int cuID) {
  int sensorID = _building->getNumberOfSensors();
  _sensorIDs.push_back(sensorID);
  Sensor sensor = Sensor(sensorPosition, cuID, sensorID);
  _building->addSensor(sensor);
  /*
  CuSensorPair csPair;
  csPair._cuID = -1; // -1 means not set to any useful value
  csPair._epID._cuID = cuID;
  csPair._epID._epIndex = sensorID;
  _building->addCuSensorPair(csPair);
  */
}

std::vector<int> Room::getSensors() {
  //std::cout << "The size of _sensorContainer is: " << _sensorContainer.size() << std::endl;
  return _sensorIDs;
}

float Room::getTemperature() {
  return _actualTemperature;
}

int Room::getID() {
  return _ID;
}

Border Room::getBorder() {
  return _roomBorder;
}

Border::Border(Point upperLeftCorner, Point lowerRightCorner) {
  _upperLeftCorner = upperLeftCorner;
  _lowerRightCorner = lowerRightCorner;
}

Point Border::getUpperLeftCorner() {
  return _upperLeftCorner;
}
Point Border::getLowerRightCorner() {
  return _lowerRightCorner;
}

//Building::Building(){}
Building::Building(Border border) {
  _border = border;
}

void Building::addControlUnit(ControlUnit cu) {
  _controlUnits.push_back(cu);
}

void Building::addSensor(Sensor s) {
  _sensors.push_back(s);
}

void Building::readSensors() {
  
  for(int i=0; i<(int)_sensors.size(); i++) {
    Sensor s = _sensors.at(i);
    float temp = s.getTemperature();
    _sensorData.at(i).push_back(temp);
  }
  // int i = 0;
  // for(Room r : _rooms) {
    // for(Sensor s : r.getSensors()) {
      // float temp = s.getTemperature();
      // _sensorData.at(i).push_back(temp);
      // i++;
    // }
  // }
}

void Building::addRoom(Point upperLeftCorner, Point lowerRightCorner, Point controlUnitLocation, Building* building) {
  int roomID = _controlUnits.size();
  ControlUnit controlUnit = ControlUnit(controlUnitLocation, this, roomID);
  _controlUnits.push_back(controlUnit);
  Room room = Room(upperLeftCorner, lowerRightCorner, roomID, building);
  _rooms.push_back(room);
}

Room* Building::getRoomByIndex(int index) {
  return &_rooms.at(index);
}

int Building::getNumberOfRooms() {
  return _rooms.size();
} 

void Building::createWallStructure() {
  for(Room r : _rooms) {
    Border b = r.getBorder();
    extractLinesFromBorder(b);
  }
}

void Building::extractLinesFromBorder(Border b) {
  Point ULC = b.getUpperLeftCorner();
  Point LRC = b.getLowerRightCorner();
  Point URC = Point(LRC.getX(), ULC.getY());
  Point LLC = Point(ULC.getX(), LRC.getY());
  std::vector<Line> lines;
  lines.push_back(Line(URC, ULC));
  lines.push_back(Line(ULC, LLC));
  lines.push_back(Line(LLC, LRC));
  lines.push_back(Line(LRC, URC));
  // The number is always 4, just like it should be...
  // std::cout << "The number of lines in \"lines\" is: " << lines.size() << std::endl;
  // int o = 0;
  bool shouldIContinue = false;
  for(Line l : lines) {
    // l.getStartPoint().print();
    // l.getEndPoint().print();
    if(_walls.size() > 0) {
      for(Line w : _walls) {
        if(l == w) {
          shouldIContinue = true;
          // Yes... But only ONCE... Which is insane...
          // std::cout << "Is anything skipped?" << std::endl;
          break;
        }

      }
      if(shouldIContinue) {
        shouldIContinue = false;
        continue;
      }
      // o++;
      _walls.push_back(l);
    } else {
        // Yes, this only happens once: 
        // std::cout << "Does this only happen once?" << std::endl;
        _walls.push_back(l);
    }
  }
  // std::cout << "The number of times that lines have been added to walls is: " << o << std::endl;
}

int Building::direction(Point a, Point b, Point c) {
  int val = ((b.getY()-a.getY()) * (c.getX()-b.getX())) - ((b.getX()-a.getX()) * (c.getY()-b.getY()));
  if(val == 0)
    return 0;
  else if(val < 0)
    return 2;
  else
    return 1;
}

bool Building::doLinesIntersect(Line l1, Line l2) {
  int dir1 = direction(l1.getStartPoint(), l1.getEndPoint(), l2.getStartPoint());
  int dir2 = direction(l1.getStartPoint(), l1.getEndPoint(), l2.getEndPoint());
  int dir3 = direction(l2.getStartPoint(), l2.getEndPoint(), l1.getStartPoint());
  int dir4 = direction(l2.getStartPoint(), l2.getEndPoint(), l1.getEndPoint());

  if((dir1 != dir2) && (dir3 != dir4)) {
    //std::cout << "The lines do intersect" << std::endl;
    return true;
  }
  else {
    //std::cout << "The lines do not intersect" << std::endl;
    return false;
  }
}

int Building::intersectWithBorders(Point p1, Point p2) {
  int numberOfIntersections = 0;
  Line line = Line(p1, p2);
  for(Line w : _walls) {
    if(doLinesIntersect(line, w))
      numberOfIntersections++;
    //std::cout << "I will do unit testing... " << std::endl;
  }
  return numberOfIntersections;
}

float Building::distanceBetweenPoints(Point p1, Point p2) {
  float dist = 0;
  dist = sqrt(pow((p2.getX() - p1.getX()), 2) + pow((p2.getY() - p1.getY()), 2));
  return dist;
}

void Building::printNumberOfWalls() {
  std::cout << "The number of walls is: " << _walls.size() << std::endl;
  std::cout << "The maximum capacity of \"walls\" is: " << _walls.capacity() << std::endl;
}

int Building::getNumberOfSensors() {
  return _sensors.size();
}

std::vector<Sensor> Building::getSensors() {
  return _sensors;
}

void Building::restartServer(Building* building) {
  _server = Server(building);
}

ControlUnit* Building::getCUByIndex(int roomID) {
  // ControlUnit cu = _controlUnits.at(roomID);
  // return &cu;
  return &_controlUnits.at(roomID);
}

void Building::averageDistance() {
  float avgData = 0;
  for(float num : _distances)
    avgData += num; 
  avgData = avgData / _distances.size();
  std::cout << "The true average distance between cus and sensors is: " << avgData << std::endl;
}

void Building::addDistance(float data) {
  _distances.push_back(data);
}

// void Building::addCuSensorPair(CuSensorPair csPair) {
  // _csPairs.push_back(csPair);
// }

Server::Server(Building* building) {
  _building = building;
  int numOfSensors = _building->getNumberOfSensors();
  int numOfControlUnits = _building->getNumberOfRooms(); // This is to get the number of controlunits in the system. This assumes that there are exactly as many CUs as there are rooms!
  _avgRssiValues = Matrix(numOfControlUnits, numOfSensors);
}

void Server::putPathLossData(std::vector<float> data, int cuID, int sensorID) {
  // std::cout << "The path loss between cu " << cuID << " and sensor "<< sensorID << " is: "<< data[0] << std::endl;
  _avgRssiValues.putData(data, cuID, sensorID);
}

void Server::processData() {
  std::vector<CuSensorPair> csPairs = _avgRssiValues.findMinPathLossCuSensorPairs();
  std::vector<CuSensorPair> csPairsTemp;

  for (CuSensorPair csPair : csPairs) {
    int sensorID = csPair._epID._epIndex;
    csPair._epID._cuID = _building->getSensors().at(sensorID).getID()._cuID;
    csPairsTemp.push_back(csPair);
  }
  for(int i=0; i<(int)csPairsTemp.size(); i++)
    // std::cout << "ECU: " << csPairsTemp[i]._cuID << "\nACU:" << csPairsTemp[i]._epID._cuID << "\nSensor index: " << csPairsTemp[i]._epID._epIndex << std::endl;
  _csPairs = csPairsTemp;
}

void Server::communicateNewCsPairs() {
  for(int i=0; i<_building->getNumberOfSensors(); i++) {
    CuSensorPair currentCsPair = _csPairs[i];
    ControlUnit* currentCu = _building->getCUByIndex(currentCsPair._cuID);
    currentCu->setConnectedSensor(currentCsPair._epID);
  }
}

int Server::checkNumberOfImproperlySetRooms() {
  std::map<int, int> roomIDs;
  for(CuSensorPair csPair : _csPairs) {
    if(!(csPair._cuID == csPair._epID._cuID)) {
      roomIDs.try_emplace(csPair._epID._cuID, 1);
    } 
  }
  return roomIDs.size();
}

void Server::run() {
  processData();
  communicateNewCsPairs();
  // std::cout << "The number of rooms not set correctly: " << checkNumberOfImproperlySetRooms() << std::endl;
}

// TODO This is good for debugging... If we need it.
float Server::checkAccuracyOfCsPairs() {
  float totalPairs = (float) _csPairs.size();
  float numberOfCorrectPairs = 0;
  // std::cout << "Total pairs: " << totalPairs << std::endl;
  for(CuSensorPair cs : _csPairs) {
    // std::cout << cs._epID._cuID << " - " << cs._cuID << std::endl;
    if (cs._cuID == cs._epID._cuID)
      numberOfCorrectPairs++;
  }
  // std::cout << "Correct pairs: " << numberOfCorrectPairs << std::endl;
  float accuracy = (numberOfCorrectPairs / totalPairs) * 100.0;
  // std::cout << "\nThe accuracy of the algorithm is: " << accuracy << "%" << std::endl;
  return accuracy;
}
