#ifndef PLACEABLE_H
#define PLACEABLE_H

#include "common.h"
#include "room.h"

class Room;
class Building;

class Placeable {
  private:
    Point _location;

  public:
    void setLocation();
    Point getLocation() const;
    Placeable();
    Placeable(Point);
};

class ControlUnit : public Placeable {
  private:
    Building* _building;
    int _cuID;
    std::vector<std::vector<float>> _sensorData;
    std::vector<endPointID> _connectedSensors;

  public:
    ControlUnit() : Placeable(){}; // Do not use the default constructor!
    ControlUnit(Point p, Building* building, int cuID) : Placeable(p) {
      _building = building;
      _cuID = cuID;
    };
    float calculatePathLoss(Placeable*);
    std::vector<float> getPathLossWithEndpoint(Placeable*, int);
    float readRSSIFromCsv(); //This should have some kind of identifier as input... Probably...
    void setConnectedSensors(std::vector<endPointID>);
    void setConnectedSensor(endPointID);
    void run();
};

class Endpoint : public Placeable {
  private:
    endPointType _type;
    endPointID _ID;
    Building* _building;

  public:
    endPointType getType();
    endPointID getID();
    void setID(int, int);
    void setBuilding(Building*);
    Building* getBuilding();

    Endpoint() : Placeable() {};
    Endpoint(Point p, int cuID, int epIndex, endPointType type) : Placeable(p) {
      _type = type;
      _ID._cuID = cuID;
      _ID._epIndex = epIndex;
    };
};

class Actuator : public Endpoint {
  private:
    char _state; // The state is open at 255 and fully closed at 0

  public:
    Actuator(){}; // Do not use the default constructor!
    Actuator(Point p, int cuID, int actuatorID) : Endpoint(p, cuID, actuatorID, actuator) {
      setState(0);
    };
    char getState();
    void setState(char);
};

class Sensor : public Endpoint {
  // to create a unique sensorID, we could give it the ID of the
  // room and the ID of itself. The combination of these two values
  // would then be unique.
  private:
    int _temp;
  public:
    Sensor(){}; // Do not use the default constructor!
    Sensor(Point p, int cuID, int sensorID) : Endpoint(p, cuID, sensorID, sensor) {}
    float getTemperature();
};

#endif